<?php

namespace Litalex\SendRequestBundle\Service;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Litalex\SendRequestBundle\Service\Interfaces\HttpClientInterface;

/**
 * Class for send request with GuzzleHttpClient.
 */
class GuzzleHttpClient implements HttpClientInterface
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritdoc}
     */
    public function send(RequestInterface $request, string $dataFormat = 'json', array $options = []) : ResponseInterface
    {
        $body = [$dataFormat => $request->getParsedBody()];

        return $this->client->send($request, array_merge($options, $body));
    }
}
