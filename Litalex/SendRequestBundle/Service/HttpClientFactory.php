<?php

namespace Litalex\SendRequestBundle\Service;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Litalex\SendRequestBundle\Service\Interfaces\HttpClientInterface;

/**
 * Class factory for create HttpClient.
 */
class HttpClientFactory
{
    /**
     * Returns instance of HttpClient.
     *
     * @param object $client
     *
     * @return HttpClientInterface
     */
    public function createClient($client) : HttpClientInterface
    {
        switch ($client) {
            case $client instanceof ClientInterface:
                $client = new GuzzleHttpClient($client);
                break;
        }

        return $client;
    }
}
