<?php

namespace Litalex\SendRequestBundle\Service;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use Litalex\SendRequestBundle\Service\Interfaces\SendRequestInterface;

/**
 * Service fo Send Request.
 */
class SendRequest implements SendRequestInterface
{
    /**
     * {@inheritdoc}
     */
    public function sendRequest(RequestInterface $request, $client, $dataFormat = 'json', $options = []) : ResponseInterface
    {
        return (new HttpClientFactory())
            ->createClient($client)
            ->send($request, $dataFormat, $options);
    }
}
