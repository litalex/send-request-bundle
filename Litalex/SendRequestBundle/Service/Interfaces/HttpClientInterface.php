<?php

namespace Litalex\SendRequestBundle\Service\Interfaces;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface for Http Client.
 */
interface HttpClientInterface
{
    /**
     * Sends Request and returns Response according to PSR-7.
     *
     * @param RequestInterface $request
     * @param string           $dataFormat
     * @param array            $options
     *
     * @return ResponseInterface
     */
    public function send(RequestInterface $request, string $dataFormat = 'json', array $options = []) : ResponseInterface;
}
