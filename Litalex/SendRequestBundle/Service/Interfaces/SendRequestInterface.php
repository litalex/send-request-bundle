<?php

namespace Litalex\SendRequestBundle\Service\Interfaces;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use Litalex\SendRequestBundle\LitalexSendRequestInterface;

/**
 * Interface for SendRequest.
 */
interface SendRequestInterface extends LitalexSendRequestInterface
{
    /**
     * @param RequestInterface $request
     *
     * @param object           $client
     * @param string           $dataFormat
     * @param array            $options
     *
     * @return ResponseInterface
     */
    public function sendRequest(RequestInterface $request, $client, $dataFormat = 'json', $options = []) : ResponseInterface;
}
