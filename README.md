Installation
==============================

Require the bundle in your composer.json file:

```json
{
    "require": {
        "litalex/send-request-bundle": "~1.0"
    }
}
```    

Register the bundle:

``` php
// app/AppKernel.php
public function registerBundles()
    {
        $bundles = [
            ...
            new Litalex\SendRequestBundle\LitalexSendRequestBundle()
            ...
        ];
    }
```

Usage
------

``` php
$client = new Client();
$sendRequestService = $this->get('litalex.send_request_service');
$sendRequestService->sendRequest($serverRequest(), $client, $dataFormat);
```
